#!/bin/bash
# gentoo-infra: infra/githooks.git:update-04-utf8
# Copyright 2017-2021 Michał Górny and others
# Distributed under the terms of the GNU General Public License v2 or later

# Author: Michał Górny <mgorny@gentoo.org>

refname=$1
oldrev=$2
newrev=$3

export LC_MESSAGES=C

# special cases
zeros=0000000000000000000000000000000000000000
# branch removal
[[ ${newrev} == "${zeros}" ]] && exit 0
rev_list_arg="${oldrev}..${newrev}"
exec 10</dev/null
# new branch; check all commits that are reachable from ${newrev}
# but not reachable from any existing ref in refs/
if [[ ${oldrev} == "${zeros}" ]]; then
    rev_list_arg="${newrev}"
    exec 10< <(git rev-parse --not --exclude="${refname}" --all)
fi

ret=0
while read commithash; do
    # verify that the commit object (including author, committer, commit
    # message) is valid UTF-8
    if ! git cat-file -p "${commithash}" | iconv -f utf8 -t utf8 &>/dev/null
    then
        echo "Commit ${commithash} contains invalid UTF-8 in the commit metadata"
        ret=1
    fi
done < <(git rev-list "${rev_list_arg}" --stdin <&10)

exit ${ret}
