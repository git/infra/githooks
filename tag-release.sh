#!/bin/bash
COMMIT=${COMMIT:=HEAD}
PACKAGENAME=githooks
VERSION=$(TZ=UTC git log --date=format-local:%Y%m%dT%H%M%SZ -1 --format=tformat:%cd "$COMMIT")
exec git tag -s "${PACKAGENAME}-${VERSION}" "$COMMIT"
